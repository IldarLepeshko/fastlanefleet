package com.Fast.Lane.Fleet.app.supercars

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.Fast.Lane.Fleet.app.MenuActivity
import com.Fast.Lane.Fleet.app.R

class HenneseyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hennesey)
        val back=findViewById<ImageView>(R.id.back)
        val menu=findViewById<ImageView>(R.id.menu)
        val next=findViewById<ImageView>(R.id.next)
        back.setOnClickListener(){
            val intent= Intent(this, SSCActivity::class.java)
            startActivity(intent)
        }
        menu.setOnClickListener(){
            val intent= Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }
        next.setOnClickListener(){
            val intent= Intent(this,PrininfActivity::class.java)
            startActivity(intent)
        }
    }
}