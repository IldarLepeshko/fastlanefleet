package com.Fast.Lane.Fleet.app.hatchbacks

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.Fast.Lane.Fleet.app.MenuActivity
import com.Fast.Lane.Fleet.app.R

class VolkGolfActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volk_golf)
        val back=findViewById<ImageView>(R.id.back)
        val menu=findViewById<ImageView>(R.id.menu)
        val next=findViewById<ImageView>(R.id.next)
        back.setOnClickListener(){
            val intent= Intent(this, FordFocusActivity::class.java)
            startActivity(intent)
        }
        menu.setOnClickListener(){
            val intent= Intent(this, MenuActivity::class.java)
            startActivity(intent)
        }
        next.setOnClickListener(){
            val intent= Intent(this, MeganeActivity::class.java)
            startActivity(intent)
        }
    }
}