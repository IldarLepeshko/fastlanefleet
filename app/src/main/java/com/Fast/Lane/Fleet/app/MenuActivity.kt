package com.Fast.Lane.Fleet.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.Fast.Lane.Fleet.app.electric.PorscheActivity
import com.Fast.Lane.Fleet.app.hatchbacks.MercActivity
import com.Fast.Lane.Fleet.app.supercars.BugattiActivity

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val supc=findViewById<ImageView>(R.id.supCars)
        val hatch=findViewById<ImageView>(R.id.hatchBack)
        val electr=findViewById<ImageView>(R.id.electrical)
        val exit=findViewById<ImageView>(R.id.exit)
        supc.setOnClickListener(){
            val intent= Intent(this,BugattiActivity::class.java)
            startActivity(intent)
        }
        hatch.setOnClickListener(){
            val intent= Intent(this, MercActivity::class.java)
            startActivity(intent)
        }
        electr.setOnClickListener(){
            val intent= Intent(this,PorscheActivity::class.java)
            startActivity(intent)
        }
        exit.setOnClickListener(){
            finishAffinity()
        }
    }
}